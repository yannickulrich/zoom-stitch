import yaml
import subprocess
from matplotlib import image
from matplotlib import pyplot
from PIL import Image
import numpy as np
import os
import glob
import re
import time


def load_image(infilename):
    img = Image.open(infilename)
    img.load()
    data = np.asarray(img, dtype='int32')
    return data


def find_box(img):
    rect = 0
    ax = pyplot.imshow(img)
    toolbar = pyplot.get_current_fig_manager().toolbar

    def on_change(ax):
        nonlocal rect
        x0, x1 = ax.get_xlim()
        y1, y0 = ax.get_ylim()
        if x0 < 0:
            return
        rect = (int(x0), int(x1), int(y0), int(y1))

    ax.axes.callbacks.connect('ylim_changed', on_change)
    toolbar.zoom()

    pyplot.show()
    return rect


def isolate(mask):
    for i in range(1, len(mask) - 1):
        if mask[i]:
            if not (mask[i-1] | mask[i+1]):
                mask[i] = False
    return mask


def getsubimg(img, quiet, rect, yline=740, zero=0):
    (x0, x1, y0, y1) = rect

    img = img[y0:y1, x0:x1, :]
    zeromask = isolate(img[:, yline, 0] == zero)

    if not quiet:
        fig, (ax1, ax2) = pyplot.subplots(ncols=2, gridspec_kw={'wspace': 0})
        ax1.imshow(img[:, :, 0])
        ax2.plot(img[:, yline, 0] / 255., np.arange(img.shape[0]))
        ax2.plot(zeromask, np.arange(img.shape[0]))
        ax2.set_aspect(1./img.shape[1])
        ax2.set_ylim(img.shape[0], 0)
        ax2.set_xlim(0, 1)

        pyplot.show()

    # Find rows
    arr = np.where(zeromask)[0]
    upper = arr[np.where(np.diff(arr) != 1)[0]+1]
    lower = arr[np.where(np.diff(arr) != 1)[0]]

    rows = zip(upper, lower)
    images = []
    for t, b in rows:
        if not quiet:
            fig, (ax1, ax2) = pyplot.subplots(
                nrows=2, gridspec_kw={'hspace': 0}
            )
            ax1.imshow(img[b:t, :, :])
            ax2.plot(np.max(img[b:t, :, 0], 0)/255.)
            ax2.plot(isolate(np.max(img[b:t, :, 0], 0) == zero))
            ax2.set_xlim(0, img.shape[1])
            ax2.set_ylim(0, 1)

        arr = np.where(isolate(np.max(img[b:t, :, 0], 0) == zero))[0]
        right = arr[np.where(np.diff(arr) != 1)[0]+1]
        left = arr[np.where(np.diff(arr) != 1)[0]]
        columns = zip(left, right)

        for l, r in columns:
            images.append(img[b:t, l:r, :])

    if not quiet:
        fig = pyplot.figure()
        for i in range(len(images)):
            pyplot.subplot(
                len(upper),
                len(images)//len(upper)+1,
                i+1
            ).imshow(images[i])
        pyplot.show()
    return images


def processgroup(imgs, part, yline, rect, zero):
    allimages = {}
    for i, f in enumerate(imgs):
        img = load_image(f)
        images = getsubimg(img, True, yline=yline, rect=rect, zero=zero)
        for j in range(len(images)):
            img = Image.fromarray(
                np.asarray(images[j][:, :, :3], 'uint8'),
                'RGB'
            )
            print('out/person%s%02d/frame%i.png' % (part, j, i))
            try:
                os.mkdir('out/person%s%02d' % (part, j))
            except FileExistsError:
                pass
            img.save('out/person%s%02d/frame%i.png' % (part, j, i))
            allimages[(j, i)] = img

    # This returns allimages[(person, frame)]
    return allimages


def resize(pilimg, new):
    if pilimg.width == new[1] and pilimg.height == new[0]:
        return np.array(pilimg)[:, :, :3]
    else:
        ratio = min(new[1] / pilimg.width, new[0] / pilimg.height)
        pilimg = pilimg.resize((
            int(ratio * pilimg.width),
            int(ratio * pilimg.height)
        ))
        pilimg = np.array(pilimg)

        img = np.zeros((new[0], new[1], 3), int)
        lt = (new[0] - pilimg.shape[0])//2
        lm = (new[1] - pilimg.shape[1])//2
        img[lt:lt+pilimg.shape[0], lm:lm+pilimg.shape[1], :] = pilimg[:, :, :3]
        return img


def assemble(all_images, good, grid, size, margin=24, pad=5):
    goodimages = [
        resize(
            all_images[(p, f)], size
        )
        for name, (p, f) in good
    ]
    gy, gx = grid
    sy, sx = size
    sy += pad
    sx += pad

    fig = pyplot.figure()
    for i, g in enumerate(goodimages):
        pyplot.subplot(gy, gx, i+1).imshow(g)
    pyplot.show()

    full = np.zeros((gy * sy + 2*margin, gx * sx + 2*margin, 3), 'uint32')
    rows = margin + sy * np.arange(gy)
    cols = margin + sx * np.arange(gx)
    i = 0

    for r in rows:
        for c in cols:
            full[r:r+(sy-pad), c:c+(sx-pad), :] = (
                goodimages[i] if i < len(goodimages) else 0
            )
            i += 1

    img = Image.fromarray(np.asarray(full[:, :, :3], 'uint8'), 'RGB')
    return img


def main(fp):
    opts = yaml.safe_load(fp)['main']

    imgs = glob.glob(opts['pattern'])
    img = load_image(imgs[0])

    if 'rect' not in opts:
        rect = find_box(img)
        print('Rect is ', rect)
    else:
        rect = tuple(opts['rect'])

    if 'yline' not in opts:
        yline = find_box(img)[2]
        print('yline is ', yline)
    else:
        yline = opts['yline']

    if 'zero' in opts:
        zero = opts['zero']
    else:
        zero = 0
    images = getsubimg(img, opts['quiet'], rect, yline=yline, zero=zero)

    all_images = processgroup(imgs, 'A', yline, rect, zero)

    if 'extra' in opts:
        for i in opts['extra']:
            img = Image.open(i['file'])
            img.load()
            all_images[tuple(i['entry'])] = img

    if 'good' in opts:
        assemble(
            all_images, opts['good'], opts['grid'], opts['size']
        ).save(opts['output'])


def focus(wid):
    subprocess.Popen(['wmctrl', '-i', '-a', wid])


def snap(folder, timeout=1, wid=None):
    if wid is None:
        proc = subprocess.Popen('xwininfo', stdout=subprocess.PIPE)
        stdout = proc.communicate()[0].decode()
        wid = re.findall('Window id: (0x[\da-f]+)', stdout)[0]

    try:
        os.mkdir(folder)
    except FileExistsError:
        pass

    i = 0
    focus(wid)
    while True:
        subprocess.Popen([
            'gnome-screenshot',
            '-w',
            '-f', "%s/photo%d.png" % (folder, i)
        ])
        time.sleep(timeout)
        i += 1


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(prog='stitch')
    parser.add_argument(
        '-i', nargs='?', type=argparse.FileType('r'),
        help='YAML file to read'
    )
    parser.add_argument(
        '--snap', nargs='?',
        help='folder to snap to'
    )
    args = parser.parse_args()
    if args.i:
        main(args.i)
    elif args.snap:
        snap(args.snap)
