# Group photos in Zoom

This tool can help you take group photos in Zoom. It was original
bodged together for [this photo](https://link.springer.com/article/10.1140%2Fepjc%2Fs10052-020-8138-9#Figa)
and more recently for [YTF](https://conference.ippp.dur.ac.uk/event/1065/).

Note: the code is terrible since both version were written under time
pressure. At any moment Zoom could change its layout and everything
would break.

I might write proper documentations some day..

Note: This project is not affiliated with Zoom in anyway and could
just as easily / laboriously be used for any other tool.
